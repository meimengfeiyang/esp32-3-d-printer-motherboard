# ESP32-3D打印机主板

#### 介绍
基于B站up:糖果大魔王开源的ESP32打印机主板V2.2做了一点修改：
1. 接口更换：将拧螺丝的接线端子换成XT60和XT30接口，相对好看，焊线需要工具
2. MOS更换：使用OpenHeat同款mos: NTMFS5C628NL(可用立创的TTG160N03GT)
3. 交互独立：板子仅支持自己做的OLED副板，集成编码器，OLED，SD卡槽
4. 接口去冗：把传感器的接口放在一起了

#### 软件架构
软件架构说明


#### 安装教程

1.  该版本板子有BUG，详情见待更新内容
2.  考虑打板白嫖情况，后续会将两块板子合并
3.  整体使用和原板子（CZR-V2.2）无太大差异

#### 使用说明

## 功能验证进度：
1. [x] 电机控制
2. [x] 大鱼CXY方向验证
3. [x] 联网功能
4. [x] 热床测试 
5. [x] 喷嘴测试 
6. [x] 风扇测试
7. [x] 打印测试
8. [  ] 长时间打印测试

## 待更新内容
1. [x] 已知595的10引脚没接到3V3，下一版本改正
2. [  ] 主控外壳设计，集成电机驱动散热风道
3. [  ] 交互板外壳设计
4. [  ] 更多类型的交互板子

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
