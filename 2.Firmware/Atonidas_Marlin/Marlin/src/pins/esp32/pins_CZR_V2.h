/**
 * Marlin 3D Printer Firmware
 * Copyright (c) 2020 MarlinFirmware [https://github.com/MarlinFirmware/Marlin]
 *
 * Based on Sprinter and grbl.
 * Copyright (c) 2011 Camiel Gubbels / Erik van der Zalm
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#pragma once

#include "env_validate.h"

#if EXTRUDERS > 2 || E_STEPPERS > 2
#error "CZR-V2.2 only supports two E Steppers. Comment out this line to continue."
#elif HOTENDS > 1
#error "CZR-V2.2 only supports one hotend / E-stepper. Comment out this line to continue."
#endif

#define BOARD_INFO_NAME "Atonidas:"
#define DEFAULT_MACHINE_NAME BOARD_INFO_NAME

//
// Limit Switches
//
#define X_STOP_PIN 34
#define Y_STOP_PIN 35
#define Z_STOP_PIN 32

//
// Enable I2S stepper stream
//
#undef I2S_STEPPER_STREAM
#define I2S_STEPPER_STREAM
#define I2S_WS 16
#define I2S_BCK 4
#define I2S_DATA 17
#undef LIN_ADVANCE // Currently, I2S stream does not work with linear advance

//
// Steppers
//
#define X_STEP_PIN 135
#define X_DIR_PIN 134
#define X_ENABLE_PIN 27
//#define X_CS_PIN                            21

#define Y_STEP_PIN 133
#define Y_DIR_PIN 132
#define Y_ENABLE_PIN X_ENABLE_PIN
//#define Y_CS_PIN                            22

#define Z_STEP_PIN 131
#define Z_DIR_PIN 130
#define Z_ENABLE_PIN X_ENABLE_PIN
//#define Z_CS_PIN                             5  // SS_PIN

#define E0_STEP_PIN 129
#define E0_DIR_PIN 128
#define E0_ENABLE_PIN X_ENABLE_PIN
//#define E0_CS_PIN                           21

// #define E1_STEP_PIN 141
// #define E1_DIR_PIN 142
// #define E1_ENABLE_PIN X_ENABLE_PIN
// //#define E1_CS_PIN                           22

// #define Z2_STEP_PIN 141
// #define Z2_DIR_PIN 142
// #define Z2_ENABLE_PIN X_ENABLE_PIN
// //#define Z2_CS_PIN                            5

//
// Temperature Sensors
//
#define TEMP_0_PIN 39 // Analog Input
//#define TEMP_1_PIN                            34  // Analog Input
#define TEMP_BED_PIN 36 // Analog Input

//
// Heaters / Fans
//
#define HEATER_0_PIN 2
#define FAN_PIN 15
#define HEATER_BED_PIN 26

//#define CONTROLLER_FAN_PIN                   147
//#define E0_AUTO_FAN_PIN                    148  // need to update Configuration_adv.h @section extruder
//#define E1_AUTO_FAN_PIN                    149  // need to update Configuration_adv.h @section extruder
//#define FAN1_PIN                             149

//
// MicroSD card
//
#define SD_MOSI_PIN 23
#define SD_MISO_PIN 19
#define SD_SCK_PIN 18
#define SDSS 5
#define USES_SHARED_SPI // SPI is shared by SD card with TMC SPI drivers

#define BTN_EN1 25
#define BTN_EN2 33
#define BTN_ENC 0
//////////////////////////
// LCDs and Controllers //
//////////////////////////
// #define LCD_PINS_RS 13
// #define LCD_PINS_ENABLE 17
// #define LCD_PINS_D4 16

// #define BEEPER_PIN 140

// #define LCD_PINS_D5 131
// #define LCD_PINS_D6 134
// #define LCD_PINS_D7 137

#if HAS_MARLINUI_U8GLIB

#if ENABLED(CR10_STOCKDISPLAY)

#elif IS_RRD_FG_SC

#else

//   #error "Only CR10_STOCKDISPLAY and REPRAP_DISCOUNT_FULL_GRAPHIC_SMART_CONTROLLER are currently supported. Comment out this line to continue."

#endif

#endif // HAS_MARLINUI_U8GLIB
